# Medusa Soil Moisture Sensor

![Medusa Soil Moisture Sensor](https://shop.edwinrobotics.com/4155-thickbox_default/medusa-soil-moisture-sensor.jpg "Medusa Soil Moisture Sensor")

[Medusa Soil Moisture Sensor](https://shop.edwinrobotics.com/modules/1240-medusa-soil-moisture-sensor.html)


The Medusa Soil Moisture Sensor can be used to determine the conductivity of the soil which is directly proportional to the amount of water present in it.


The Medusa Soil Moisture Sensor has a gold plated finishing to increase the longevity of the sensor, it can further be improved by turning on the sensor only when you need to get a reading. This can be done using the jumper setting present on the PCB. Check out our upcoming hookup guide for more information on this. 


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.